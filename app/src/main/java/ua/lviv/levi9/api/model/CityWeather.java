package ua.lviv.levi9.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

/**
 * @author nk
 */

public class CityWeather implements Serializable/*Not that bad, believe me*/ {
  @SerializedName("coord") public Coord coord;
  @SerializedName("weather") public List<Weather> weather = null;
  @SerializedName("base") public String base;
  @SerializedName("main") public Main main;
  @SerializedName("visibility") public int visibility;
  @SerializedName("wind") public Wind wind;
  @SerializedName("clouds") public Clouds clouds;
  @SerializedName("dt") public long dt;
  @SerializedName("sys") public Sys sys;
  @SerializedName("id") public int id;
  @SerializedName("name") public String name;
  @SerializedName("cod") public int cod;

  public String getName() {
    return name;
  }

  public static class Weather implements Serializable {
    @SerializedName("id") public int id;
    @SerializedName("main") public String main;
    @SerializedName("description") public String description;
    @SerializedName("icon") public String icon;
  }

  public static class Wind implements Serializable {

    @SerializedName("speed") public double speed;
    @SerializedName("deg") public double deg;
  }

  public static class Clouds implements Serializable {

    @SerializedName("all") public int all;
  }

  public static class Coord implements Serializable {
    @SerializedName("lon") @Expose public double lon;
    @SerializedName("lat") @Expose public double lat;
  }

  public static class Main implements Serializable {

    @SerializedName("temp") @Expose public double temp;
    @SerializedName("pressure") @Expose public double pressure;
    @SerializedName("humidity") @Expose public double humidity;
    @SerializedName("temp_min") @Expose public double tempMin;
    @SerializedName("temp_max") @Expose public double tempMax;
  }

  public static class Sys implements Serializable {

    @SerializedName("type") @Expose public int type;
    @SerializedName("id") @Expose public int id;
    @SerializedName("message") @Expose public double message;
    @SerializedName("country") @Expose public String country;
    @SerializedName("sunrise") @Expose public long sunrise;
    @SerializedName("sunset") @Expose public long sunset;
  }

  @Override public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    CityWeather that = (CityWeather) o;

    return id == that.id;
  }

  @Override public int hashCode() {
    return id;
  }
}

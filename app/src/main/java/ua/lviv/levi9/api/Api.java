package ua.lviv.levi9.api;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import ua.lviv.levi9.api.model.CityWeather;
import ua.lviv.levi9.api.model.WeatherList;
import ua.lviv.levi9.http.HttpClient;
import ua.lviv.levi9.http.NetworkDispatcher;
import ua.lviv.levi9.http.Parser;
import ua.lviv.levi9.util.Strings;

/**
 * @author nk
 */

public class Api {
  private static final String TAG = "Api";
  private static final String APP_ID = "c6e381d8c7ff98f0fee43775817cf6ad";
  private static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";

  private NetworkDispatcher networkDispatcher;

  public Api(HttpClient httpClient, Parser parser) {
    this.networkDispatcher = new NetworkDispatcher(httpClient, parser);
  }

  public NetworkDispatcher.Call fetchWeatherInCities(List<Long> cities,
      NetworkDispatcher.Callback<WeatherList> callback) {
    String ids = Strings.join(cities, ",");
    String url = BASE_URL + "group";

    HashMap<String, String> params = new HashMap<>();
    params.put("id", ids);
    params.put("units", "metric");
    params.put("appId", APP_ID);

    HttpClient.Request request = new HttpClient.Request.Builder().url(url)
        .contentType("application/json")
        .params(params)
        .method(HttpClient.HttpMethod.GET)
        .build();

    return networkDispatcher.execute(request, WeatherList.class, new WeakReference<>(callback));
  }

  public NetworkDispatcher.Call fetchWeatherByCityLocation(double lat, double lng,
      NetworkDispatcher.Callback<CityWeather> callback) {
    String url = BASE_URL + "weather";

    HashMap<String, String> params = new HashMap<>();
    params.put("units", "metric");
    params.put("lat", String.valueOf(lat));
    params.put("lon", String.valueOf(lng));
    params.put("appId", APP_ID);

    HttpClient.Request request = new HttpClient.Request.Builder().url(url)
        .contentType("application/json")
        .params(params)
        .method(HttpClient.HttpMethod.GET)
        .build();

    return networkDispatcher.execute(request, CityWeather.class, new WeakReference<>(callback));
  }
}

package ua.lviv.levi9.api.model;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * @author nk
 */

public class WeatherList {
  @SerializedName("cnt") String cnt;
  @SerializedName("list") List<CityWeather> list;

  public String getCnt() {
    return cnt;
  }

  public List<CityWeather> getList() {
    return list;
  }
}

package ua.lviv.levi9.ui;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import ua.lviv.levi9.R;
import ua.lviv.levi9.WeatherApp;
import ua.lviv.levi9.api.Api;
import ua.lviv.levi9.api.model.CityWeather;
import ua.lviv.levi9.http.NetworkDispatcher;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

/**
 * @author nk
 */

public class MapLocationPickerFragment extends SupportMapFragment implements OnMapReadyCallback {
  private static final String TAG = "MapLocationPicker";

  public static final int DELAY_SEARCH = 1500; //1.5 sec

  private GoogleMap googleMap;
  private Handler handler = new Handler(Looper.getMainLooper());

  private Runnable fetchTask = new Runnable() {
    @Override public void run() {
      if (googleMap != null) {
        fetchCityWithLocation(googleMap.getCameraPosition());
      }
    }
  };

  private Api api;
  private NetworkDispatcher.Call call;
  private Snackbar snackbar;

  @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    api = WeatherApp.app().api();
    getMapAsync(this);
    snackbar = Snackbar.make(getView(), R.string.pick_city_on_map, Snackbar.LENGTH_INDEFINITE);
    snackbar.show();
    addStaticImage((FrameLayout) view);
  }

  private void addStaticImage(FrameLayout view) {
    // add image view programmatically
    final AppCompatImageView image = new AppCompatImageView(getContext());
    image.setImageResource(R.drawable.ic_place_black_24dp);

    RelativeLayout relativeLayout = new RelativeLayout(getContext());
    relativeLayout.setLayoutParams(new FrameLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT));

    RelativeLayout.LayoutParams params =
        new RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
    params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
    image.setLayoutParams(params);

    // bottom of the image marker should be exactly in the middle of the map
    image.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
      @Override public boolean onPreDraw() {
        image.getViewTreeObserver().removeOnPreDrawListener(this);
        image.setPadding(0, 0, 0, image.getHeight());
        return true;
      }
    });

    relativeLayout.addView(image);
    view.addView(relativeLayout);
  }

  @Override public void onMapReady(GoogleMap map) {
    googleMap = map;
    googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
      @Override public void onCameraIdle() {
        handler.postDelayed(fetchTask, DELAY_SEARCH);
      }
    });
  }

  private void fetchCityWithLocation(CameraPosition cameraPosition) {
    if (call != null) {
      call.cancel();
    }

    call = api.fetchWeatherByCityLocation(cameraPosition.target.latitude,
        cameraPosition.target.longitude, new NetworkDispatcher.Callback<CityWeather>() {
          @Override public void onSuccess(CityWeather cityWeather) {
            showSnackBar(cityWeather);
          }

          @Override public void onFailure(Exception e) {
            Log.e(TAG, "onFailure: ", e);
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
          }
        });
  }

  @Override public void onDestroyView() {
    handler.removeCallbacks(fetchTask);
    if (call != null) {
      call.cancel();
    }
    super.onDestroyView();
  }

  void showSnackBar(final CityWeather cityWeather) {
    boolean foundCity = cityWeather.name != null && cityWeather.sys.country != null;
    String message;
    if (foundCity) {
      message = cityWeather.name + ", " + cityWeather.sys.country;
    } else {
      message = getResources().getString(R.string.not_found);
    }

    snackbar.setText(message);
    if (foundCity) {
      snackbar.setAction(R.string.add_to_bookmark, new View.OnClickListener() {
        @Override public void onClick(View view) {
          addCity(cityWeather);
        }
      });
    } else {
      snackbar.setAction(null, null);
    }
  }

  private void addCity(CityWeather cityWeather) {
    Fragment fragment =
        getActivity().getSupportFragmentManager().findFragmentByTag(HomeFragment.TAG);
    if (fragment instanceof HomeFragment) {
      ((HomeFragment) fragment).onCityAdded(cityWeather);
    }
    getActivity().getSupportFragmentManager().popBackStack();
  }

  @Override public void onDestroy() {
    super.onDestroy();
    snackbar.dismiss();
  }
}

package ua.lviv.levi9.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.TypedValue;
import android.view.View;
import android.widget.FrameLayout;
import ua.lviv.levi9.R;
import ua.lviv.levi9.persistance.UserSettings;
import ua.lviv.levi9.WeatherApp;

import static ua.lviv.levi9.persistance.UserSettings.CELSIUS_SYMBOL;
import static ua.lviv.levi9.util.Dimens.dp2px;

/**
 * @author nk
 */

public class SettingsFragment extends PreferenceFragmentCompat {
  private static final String TAG = "SettingsFragment";
  UserSettings userSettings;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    // add toolbar programmatically
    addToolbar(view);
  }

  @Override public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
    addPreferencesFromResource(R.xml.settings);
    userSettings = WeatherApp.app().userSettings();
    final ListPreference listPreference =
        (ListPreference) getPreferenceManager().findPreference("unit");

    updateUnitTitle(listPreference);

    listPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
      @Override public boolean onPreferenceChange(Preference preference, Object newValue) {
        userSettings.setUseCelsius(CELSIUS_SYMBOL.equals(newValue));

        Fragment fragment =
            getActivity().getSupportFragmentManager().findFragmentByTag(HomeFragment.TAG);
        if (fragment instanceof HomeFragment) {
          ((HomeFragment) fragment).refreshCities();
        }
        updateUnitTitle(listPreference);
        return true;
      }
    });

    getPreferenceManager().findPreference("about")
        .setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
          @Override public boolean onPreferenceClick(Preference preference) {
            getActivity().getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.content, new AboutFragment())
                .addToBackStack("about")
                .commit();
            return true;
          }
        });
  }

  private void updateUnitTitle(ListPreference preference) {
    preference.setTitle(Html.fromHtml(getString(R.string.unit)
        + " <b>"
        + (userSettings.useCelsius() ? " \u2103" : " \u2109")
        + "</b>"));
  }

  private void addToolbar(View view) {
    FrameLayout viewGroup = (FrameLayout) view.findViewById(android.R.id.list_container);
    View recycler = viewGroup.getChildAt(0);

    Toolbar toolbar = new Toolbar(getContext());
    toolbar.setTitle(R.string.settings);
    toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
    toolbar.setTitleTextColor(Color.WHITE);
    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        getActivity().getSupportFragmentManager().popBackStack();
      }
    });
    int actionBarHeight = actionBarHeight();
    toolbar.setLayoutParams(
        new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, actionBarHeight));

    toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
    viewGroup.addView(toolbar);
    recycler.setPadding(recycler.getPaddingLeft(), actionBarHeight, recycler.getPaddingRight(),
        recycler.getPaddingRight());
    view.setBackgroundColor(Color.WHITE);
  }

  private int actionBarHeight() {
    TypedValue tv = new TypedValue();
    if (getActivity().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
      return TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
    }
    return dp2px(getContext(), 65);
  }
}

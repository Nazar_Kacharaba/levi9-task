package ua.lviv.levi9.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import ua.lviv.levi9.R;

public class MainActivity extends AppCompatActivity {

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    if (savedInstanceState == null) {
      getSupportFragmentManager().beginTransaction()
          .add(R.id.content, new HomeFragment(), HomeFragment.TAG)
          .commit();
    }
  }
}

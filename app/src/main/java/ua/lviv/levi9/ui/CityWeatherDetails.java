package ua.lviv.levi9.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.text.SimpleDateFormat;
import java.util.Date;
import ua.lviv.levi9.R;
import ua.lviv.levi9.api.model.CityWeather;

/**
 * @author nk
 */

public class CityWeatherDetails extends Fragment {
  static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm");

  private static final String ARG_CITY_WEATHER = "c_w";
  private CityWeather weather;
  private Toolbar toolbar;

  public static CityWeatherDetails newInstance(CityWeather weather) {
    Bundle args = new Bundle();
    CityWeatherDetails fragment = new CityWeatherDetails();
    args.putSerializable(ARG_CITY_WEATHER, weather);
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    weather = (CityWeather) getArguments().getSerializable(ARG_CITY_WEATHER);
  }

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_weather_details, container, false);
  }

  @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    toolbar = (Toolbar) view.findViewById(R.id.toolbar);
    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        getActivity().getSupportFragmentManager().popBackStack();
      }
    });

    toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
    TextView info = (TextView) view.findViewById(R.id.tv_information);
    //temperature, humidity, rain chances and wind information
    info.setText(Html.fromHtml("Weather information in <big><b>" + weather.name + "</big></b>"));
    info.append("\n");
    info.append("Temperature: "
        + weather.main.temp
        + ", min "
        + weather.main.tempMin
        + ", max "
        + weather.main.tempMax);
    info.append("\n");
    info.append("Humidity: " + weather.main.humidity);
    info.append("\n");
    info.append("Wind speed: " + weather.wind.speed);
    info.append("\n");
    info.append("Daylight: "
        + DATE_FORMAT.format(new Date(weather.sys.sunrise * 1000))
        + "-"
        + DATE_FORMAT.format(new Date(weather.sys.sunset * 1000)));
  }
}

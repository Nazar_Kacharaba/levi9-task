package ua.lviv.levi9.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import ua.lviv.levi9.R;
import ua.lviv.levi9.api.model.CityWeather;
import ua.lviv.levi9.image.ImageLoader;
import ua.lviv.levi9.persistance.UserSettings;

import static android.text.Html.fromHtml;
import static ua.lviv.levi9.util.UnitConverter.celsiusToFahrenheit;

/**
 * @author nk
 */

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.ViewHolder> {
  static final SimpleDateFormat FORMAT_TIME = new SimpleDateFormat("EEE, d MMM HH:mm");
  private final UserSettings userSettings;

  interface OnCityClickListener {

    void onCityClick(CityWeather cityWeather);

    void onCityRemoved(CityWeather cityWeather);
  }

  private List<CityWeather> cities = new ArrayList<>();

  private final OnCityClickListener listener;
  private final ImageLoader imageLoader;

  public CityAdapter(OnCityClickListener listener, ImageLoader imageLoader, UserSettings settings) {
    this.listener = listener;
    this.imageLoader = imageLoader;
    this.userSettings = settings;
  }

  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city, parent, false);
    return new ViewHolder(view);
  }

  public void addAll(List<CityWeather> weathers) {
    this.cities = weathers;
    notifyDataSetChanged();
  }

  public void addCity(CityWeather city) {
    cities.add(city);
    notifyItemInserted(cities.size() - 1);
  }

  public void removeCity(CityWeather cityWeather) {
    int index = cities.indexOf(cityWeather);
    if (index != -1) {
      cities.remove(index);
      notifyItemRemoved(index);
    }
  }

  @Override public void onBindViewHolder(ViewHolder holder, int position) {
    final CityWeather city = cities.get(position);
    holder.detail.setText(fromHtml(makeDetailString(city)));

    String url = urlForIcon(city);
    if (url != null) {
      imageLoader.load(holder.weatherIcon, url);
    }

    holder.itemView.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        listener.onCityClick(city);
      }
    });
    holder.closeBt.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        listener.onCityRemoved(city);
      }
    });
  }

  private String makeDetailString(CityWeather city) {

    StringBuilder string = new StringBuilder();
    string.append("<big><b>")
        .append(city.name)
        .append("</b></big>")
        .append(", ")
        .append("<small>")
        .append(FORMAT_TIME.format(new Date(city.dt * 1000)))
        .append("</small>")
        .append("<br>")
        .append("<big>")
        .append((int) (userSettings.useCelsius() ? city.main.temp
            : celsiusToFahrenheit(city.main.temp)))
        .append(userSettings.useCelsius() ? " \u2103" : " \u2109")
        .append("</big>")
        .append(", ")
        .append(city.weather != null && !city.weather.isEmpty() ? city.weather.get(0).main : "");

    return string.toString();
  }

  private String urlForIcon(CityWeather city) {
    if (city.weather != null && city.weather.size() > 0) {
      return "http://openweathermap.org/img/w/" + city.weather.get(0).icon + ".png";
    }
    return null;
  }

  @Override public int getItemCount() {
    return cities.size();
  }

  public static class ViewHolder extends RecyclerView.ViewHolder {
    private final TextView detail;
    private final ImageView weatherIcon;
    private final View closeBt;

    public ViewHolder(View itemView) {
      super(itemView);
      detail = (TextView) itemView.findViewById(R.id.tv_weather_info);
      closeBt = itemView.findViewById(R.id.iv_close);
      weatherIcon = (ImageView) itemView.findViewById(R.id.icon);
    }
  }
}

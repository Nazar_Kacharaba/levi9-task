package ua.lviv.levi9.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import ua.lviv.levi9.R;
import ua.lviv.levi9.WeatherApp;
import ua.lviv.levi9.api.Api;
import ua.lviv.levi9.api.model.CityWeather;
import ua.lviv.levi9.api.model.WeatherList;
import ua.lviv.levi9.http.NetworkDispatcher;
import ua.lviv.levi9.image.ImageLoader;
import ua.lviv.levi9.persistance.CitiesBookmark;
import ua.lviv.levi9.persistance.UserSettings;

/**
 * @author nk
 */

public class HomeFragment extends Fragment
    implements CityAdapter.OnCityClickListener, NetworkDispatcher.Callback<WeatherList> {
  public static final String TAG = "HomeFragment";
  private RecyclerView recyclerView;
  private CityAdapter adapter;
  private CitiesBookmark bookmark;
  private NetworkDispatcher.Call call;

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
    Api api = WeatherApp.app().api();
    ImageLoader imageLoader = WeatherApp.app().imageLoader();
    UserSettings userSettings = WeatherApp.app().userSettings();

    adapter = new CityAdapter(this, imageLoader, userSettings);
    bookmark = WeatherApp.app().bookmark();
    List<CityWeather> all = bookmark.getAll();
    adapter.addAll(all);

    if (!all.isEmpty()) {
      List<Long> ids = new ArrayList<>(all.size());
      for (CityWeather cityWeather : all) {
        ids.add((long) cityWeather.id);
      }
      if (call != null) {
        call.cancel();
      }

      call = api.fetchWeatherInCities(ids, this);
    }
  }

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_home, container, false);
  }

  @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    recyclerView = (RecyclerView) view.findViewById(R.id.rv_recycler_view);
    showExplanation(adapter.getItemCount() == 0);

    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    recyclerView.setAdapter(adapter);

    Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
    toolbar.setTitle(R.string.location);
    toolbar.inflateMenu(R.menu.menu_home_fragment);
    toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
      @Override public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId() == R.id.menu_add) {
          if (adapter.getItemCount() == 20) {
            Toast.makeText(getContext(), R.string.no_more_20_cities, Toast.LENGTH_SHORT).show();
          } else {
            getActivity().getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.content, new MapLocationPickerFragment())
                .addToBackStack("map")
                .commit();
          }
          return true;
        } else if (item.getItemId() == R.id.menu_settings) {
          getActivity().getSupportFragmentManager()
              .beginTransaction()
              .add(R.id.content, new SettingsFragment())
              .addToBackStack("settings")
              .commit();
        }
        return false;
      }
    });
  }

  private void showExplanation(boolean show) {
    if (getView() == null) {
      return;
    }

    View explanationLayout = getView().findViewById(R.id.ll_explanation_layout);

    if (show) {
      explanationLayout.setVisibility(View.VISIBLE);
      recyclerView.setVisibility(View.GONE);
    } else {
      explanationLayout.setVisibility(View.GONE);
      recyclerView.setVisibility(View.VISIBLE);
    }
  }

  @Override public void onDestroy() {
    super.onDestroy();
    if (call != null) {
      call.cancel();
    }
  }

  public void refreshCities() {
    adapter.notifyDataSetChanged();
  }

  @Override public void onCityClick(CityWeather cityWeather) {
    getActivity().getSupportFragmentManager()
        .beginTransaction()
        .add(R.id.content, CityWeatherDetails.newInstance(cityWeather))
        .addToBackStack("details")
        .commit();
  }

  @Override public void onCityRemoved(CityWeather cityWeather) {
    bookmark.remove(cityWeather.id);
    adapter.removeCity(cityWeather);
    showExplanation(adapter.getItemCount() == 0);
  }

  public void onCityAdded(CityWeather cityWeather) {
    boolean add = bookmark.add(cityWeather);
    if (add) {
      adapter.addCity(cityWeather);
    }
    showExplanation(adapter.getItemCount() == 0);
  }

  @Override public void onSuccess(WeatherList weatherList) {
    List<CityWeather> list = weatherList.getList();
    bookmark.addAll(list);
    adapter.addAll(list);
  }

  @Override public void onFailure(Exception e) {
    Toast.makeText(getContext(), R.string.could_not_update, Toast.LENGTH_SHORT).show();
  }
}

package ua.lviv.levi9.persistance;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import ua.lviv.levi9.api.model.CityWeather;
import ua.lviv.levi9.http.Parser;

/**
 * @author nk
 */

public class CitiesBookmark {
  private Parser parser;
  private SharedPreferences sharedPreferences;

  public CitiesBookmark(Context context, Parser parser) {
    this.parser = parser;
    this.sharedPreferences = context.getSharedPreferences("bookmark", Context.MODE_PRIVATE);
  }

  public boolean add(CityWeather data) {
    boolean alreadyExist = sharedPreferences.contains(String.valueOf(data.id));
    String json = parser.toJson(data);
    // if it exist, just update it
    sharedPreferences.edit().putString(String.valueOf(data.id), json).apply();
    return !alreadyExist;
  }

  public void addAll(final List<CityWeather> list) {
    AsyncTask.SERIAL_EXECUTOR.execute(new Runnable() {
      @SuppressLint("ApplySharedPref") @Override public void run() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        for (CityWeather cityWeather : list) {
          editor.putString(String.valueOf(cityWeather.id), parser.toJson(cityWeather)).commit();
        }
      }
    });
  }

  public void remove(int id) {
    sharedPreferences.edit().remove(String.valueOf(id)).apply();
  }

  public List<CityWeather> getAll() {
    Map<String, ?> all = sharedPreferences.getAll();
    Collection<?> values = all.values();
    ArrayList<CityWeather> list = new ArrayList<>(values.size());
    for (Object value : values) {
      if (value instanceof String) {
        list.add(parser.fromJson((String) value, CityWeather.class));
      }
    }
    return list;
  }
}

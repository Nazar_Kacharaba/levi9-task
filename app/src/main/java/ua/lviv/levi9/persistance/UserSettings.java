package ua.lviv.levi9.persistance;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.preference.PreferenceManager;

/**
 * @author nk
 */

public class UserSettings {
  private static final String TAG = "UserSettings";
  public static final String CELSIUS_SYMBOL = "℃";
  private boolean useCelsius;

  public UserSettings(Context context) {
    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
    this.useCelsius = sp.getString("unit", CELSIUS_SYMBOL).equals(CELSIUS_SYMBOL);
  }

  public boolean useCelsius() {
    return useCelsius;
  }

  public void setUseCelsius(boolean useCelsius) {
    this.useCelsius = useCelsius;
  }

  public static String unit(UserSettings settings) {
    return settings.useCelsius() ? "metric" : "imperial";
  }
}

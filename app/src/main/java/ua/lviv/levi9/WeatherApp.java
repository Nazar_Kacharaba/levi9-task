package ua.lviv.levi9;

import android.app.Application;
import com.google.gson.Gson;
import ua.lviv.levi9.api.Api;
import ua.lviv.levi9.http.HttpClient;
import ua.lviv.levi9.http.Parser;
import ua.lviv.levi9.http.ParserFactory;
import ua.lviv.levi9.image.ImageLoader;
import ua.lviv.levi9.image.StupidImageLoader;
import ua.lviv.levi9.persistance.CitiesBookmark;
import ua.lviv.levi9.persistance.UserSettings;

/**
 * @author nk
 */

public class WeatherApp extends Application {
  static WeatherApp app;

  private Api api;
  private CitiesBookmark bookmark;
  private ImageLoader imageLoader;
  private UserSettings userSettings;

  @Override public void onCreate() {
    super.onCreate();
    app = this;

    Parser parser = ParserFactory.getParser(new Gson());
    HttpClient httpClient = new HttpClient();

    // global objects that can be reused in every ui component
    bookmark = new CitiesBookmark(getApplicationContext(), parser);
    api = new Api(httpClient, parser);
    imageLoader = new StupidImageLoader(getApplicationContext(), httpClient);
    userSettings = new UserSettings(getApplicationContext());
  }

  public static WeatherApp app() {
    return app;
  }

  public Api api() {
    return api;
  }

  public CitiesBookmark bookmark() {
    return bookmark;
  }

  public ImageLoader imageLoader() {
    return imageLoader;
  }

  public UserSettings userSettings() {
    return userSettings;
  }
}

package ua.lviv.levi9.http;

/**
 * @author nk
 */

public interface Parser {
  <T> T fromJson(String str, Class<? extends T> clazz);

  <T> String toJson(T object);
}

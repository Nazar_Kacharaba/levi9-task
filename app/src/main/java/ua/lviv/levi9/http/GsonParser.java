package ua.lviv.levi9.http;

import com.google.gson.Gson;

/**
 * @author nk
 */

/*package private*/class GsonParser implements Parser {

  private final Gson gson;

  public GsonParser(Gson gson) {
    this.gson = gson;
  }

  @Override public <T> T fromJson(String str, Class<? extends T> clazz) {
    return gson.fromJson(str, clazz);
  }

  @Override public <T> String toJson(T object) {
    return gson.toJson(object);
  }
}

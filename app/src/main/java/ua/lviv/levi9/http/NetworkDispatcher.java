package ua.lviv.levi9.http;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author nk
 */

public final class NetworkDispatcher {
  private final boolean LOG = true;
  final HttpClient httpClient;
  final Parser parser;
  private final Handler mainHandler = new Handler(Looper.getMainLooper());
  private ExecutorService service =
      new ThreadPoolExecutor(0, 6, 60, TimeUnit.SECONDS, new SynchronousQueue<Runnable>(),
          new ThreadFactory() {
            @Override public Thread newThread(Runnable runnable) {
              return new Thread(runnable, "network");
            }
          });

  public NetworkDispatcher(HttpClient httpClient, Parser serializer) {
    this.httpClient = httpClient;
    this.parser = serializer;
  }

  public interface Callback<Response> {
    void onSuccess(Response response);

    void onFailure(Exception e);
  }

  public final static class Call {

    private final CancelableTask runnable;

    private Call(CancelableTask runnable) {
      this.runnable = runnable;
    }

    public void cancel() {
      runnable.cancel();
    }
  }

  public <T> Call execute(final HttpClient.Request request, final Class<T> clazz,
      final WeakReference<Callback<T>> callback) {

    CancelableTask runnable = createRunnable(request, clazz, callback);

    service.execute(runnable);

    return new Call(runnable);
  }

  private final <T> CancelableTask createRunnable(final HttpClient.Request request,
      final Class<T> clazz, final WeakReference<Callback<T>> callback) {
    return new CancelableTask() {
      @Override public void run() {
        if (isCanceled) return;

        final HttpClient.Response response;
        try {
          response = httpClient.execute(request);
        } catch (final IOException e) {
          mainHandler.post(new Runnable() {
            @Override public void run() {
              if (!isCanceled && callback.get() != null) {
                callback.get().onFailure(e);
              }
            }
          });
          return;
        }
        if (isCanceled) return;

        if (response.isOk()) {
          try {
            String string = response.string();

            if (LOG) Log.d("HttpClient", "-->" + request.method + " " + request.url.toString());
            if (LOG) Log.d("HttpClient", "<--" + string);

            final T object = parser.fromJson(string, clazz);

            mainHandler.post(new Runnable() {
              @Override public void run() {
                if (!isCanceled && callback.get() != null) {
                  try {
                    callback.get().onSuccess(object);
                  } catch (Exception e) {
                    callback.get().onFailure(e);
                  }
                }
              }
            });
          } catch (final Exception e) {
            mainHandler.post(new Runnable() {
              @Override public void run() {
                if (!isCanceled && callback.get() != null) {
                  callback.get().onFailure(e);
                }
              }
            });
          }
        } else {
          mainHandler.post(new Runnable() {
            @Override public void run() {
              if (!isCanceled && callback.get() != null) {
                callback.get().onFailure(new Exception(response.string()));
              }
            }
          });
        }
      }
    };
  }

  /** Cancelable runnable */
  static abstract class CancelableTask implements Runnable {
    volatile boolean isCanceled;

    public void cancel() {
      this.isCanceled = true;
    }
  }
}

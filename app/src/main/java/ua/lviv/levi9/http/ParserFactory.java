package ua.lviv.levi9.http;

import com.google.gson.Gson;

/**
 * @author nk
 */

public class ParserFactory {
  public static Parser getParser(Gson gson) {
    return new GsonParser(gson);
  }
}

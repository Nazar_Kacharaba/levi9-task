package ua.lviv.levi9.util;

/**
 * @author nk
 */

public class UnitConverter {

  public static int celsiusToFahrenheit(double celsius) {
    return (int) (celsius * 1.8) + 32;
  }
}

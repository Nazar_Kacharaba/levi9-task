package ua.lviv.levi9.util;

import android.content.Context;
import android.graphics.Point;
import android.util.TypedValue;
import android.view.WindowManager;

/**
 * @author nk
 */
public final class Dimens {
  static Context context;
  private static Point deviceSize;

  public static int deviceHeight() {
    return deviceSize().y;
  }

  public static int deviceWidth() {
    return deviceSize().x;
  }

  private static Point deviceSize() {
    if (context == null) {
      throw new NullPointerException("Call Dimens.init(context) first");
    }
    if (deviceSize != null) {
      return deviceSize;
    }

    WindowManager systemService = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
    deviceSize = new Point();
    systemService.getDefaultDisplay().getSize(deviceSize);
    return deviceSize;
  }

  public static int dp2px(int dp) {
    if (context == null) {
      throw new NullPointerException("Call Dimens.init(context) first");
    }

    return dp2px(context, dp);
  }

  public static int dp2px(Context context, int dp) {
    return (int) (dp * context.getResources().getDisplayMetrics().density);
  }

  public static float px2sp(int px) {
    return px / context.getResources().getDisplayMetrics().scaledDensity;
  }

  public static void init(Context context) {
    Dimens.context = context;
  }

  public static float sp2px(float value) {
    if (context == null) {
      throw new NullPointerException("Call Dimens.init(context) first");
    }
    return TypedValue.applyDimension(2, value, context.getResources().getDisplayMetrics());
  }
}

package ua.lviv.levi9.util;

import java.util.List;

/**
 * @author nk
 */

public final class Strings {
  static public String join(List list, String conjunction) {
    StringBuilder sb = new StringBuilder();
    boolean first = true;
    for (Object item : list) {
      if (first) {
        first = false;
      } else {
        sb.append(conjunction);
      }
      sb.append(String.valueOf(item));
    }
    return sb.toString();
  }
}

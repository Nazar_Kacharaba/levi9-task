package ua.lviv.levi9.image;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.widget.ImageView;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import ua.lviv.levi9.http.HttpClient;

/**
 * @author nk
 */

public class StupidImageLoader implements ImageLoader {
  private static final String TAG = "StupidImageLoader";
  public static final int NUMBER_OF_THREADS = 2;
  final HttpClient client;
  final LruCache<String, Bitmap> cache;
  final Handler mainHandler = new Handler(Looper.getMainLooper());
  private ExecutorService service =
      Executors.newFixedThreadPool(NUMBER_OF_THREADS, new ThreadFactory() {
        @Override public Thread newThread(@NonNull Runnable r) {
          return new Thread(r, "image-loader");
        }
      });

  public StupidImageLoader(Context context, HttpClient client) {
    this.client = client;

    final int memClass =
        ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();

    // Use 1/8th of the available memory for this memory cache.
    int cacheSize = 1024 * 1024 * memClass / 8;

    this.cache = new LruCache<String, Bitmap>(cacheSize) {
      @Override protected int sizeOf(String key, Bitmap bitmap) {
        return bitmap.getByteCount();
      }
    };
  }

  @Override public void load(final ImageView view, final String url) {
    Bitmap bitmap = cache.get(url);
    if (bitmap != null) {
      view.setImageBitmap(bitmap);
    } else {
      view.setTag(url);
      service.submit(new Runnable() {
        @Override public void run() {
          Bitmap cached = cache.get(url);
          if (cached != null) {
            setBitmapIntoTheView(cached, url, view);
          } else {
            Bitmap bitmap = null;
            InputStream stream = null;
            try {
              stream = client.execute(new HttpClient.Request.Builder().url(url)
                  .method(HttpClient.HttpMethod.GET)
                  .build()).buffer();
              bitmap = BitmapFactory.decodeStream(stream);
            } catch (Exception e) {
              Log.e(TAG, "Image was not loaded, url " + url, e);
            } finally {
              closeQuietly(stream);
            }

            if (bitmap != null) {
              cache.put(url, bitmap);
              setBitmapIntoTheView(bitmap, url, view);
            }
          }
        }
      });
    }
  }

  private void closeQuietly(InputStream stream) {
    if (stream != null) {
      try {
        stream.close();
      } catch (IOException ignore) {
      }
    }
  }

  private void setBitmapIntoTheView(final Bitmap bitmap, String url, final ImageView view) {
    if (url.equals(view.getTag())) {
      mainHandler.post(new Runnable() {
        @Override public void run() {
          view.setImageBitmap(bitmap);
        }
      });
    }
  }
}

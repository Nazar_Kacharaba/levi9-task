package ua.lviv.levi9.image;

import android.widget.ImageView;

/**
 * @author nk
 */

public interface ImageLoader {
  public void load(ImageView view, String url);
}
